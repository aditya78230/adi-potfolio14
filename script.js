function toggleMenu() {
  const menu = document.querySelector(".menu-links");
  const icon = document.querySelector(".hamburger-icon");
  menu.classList.toggle("open");
  icon.classList.toggle("open");
}
function toggleDarkMode() {
  const body = document.body;
  const darkModeToggle = document.getElementById('dark-mode-toggle');
  body.classList.toggle("dark-mode");
  
  if (body.classList.contains("dark-mode")) {
    darkModeToggle.innerText = "Light Mode";
  } else {
    darkModeToggle.innerText = "Dark Mode";
  }
}



